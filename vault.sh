#!/bin/sh -e
#
# Copyright (c) 2020-2021 Marián Koprivňanský
#
# This file is licensed under the GPL v2, or a later version
# at the discretion of Marián.
#

print_help () {
    printf 'usage: vault.sh [OPTIONS] [VAULT_DATA] [INPUT]\n\nDESCRIPTION\n    The script decrypts secret from vault format or encrypts to vault format.\n\n    The options are as follows:\n\n    -d, --decrypt\n            Decrypting mode (default mode).\n\n    -e, --encrypt\n            Encrypting mode.\n\n    -f, --file FILE\n            Selects an input/output file from/to which the value of the secret\n            will be loaded/saved. If an FILE is not specified, then instead of\n            file will be used standard input/output. The default value is empty.\n\n    -g, --group GROUP\n            In encryption mode specifying a group for the secret.\n            The group 0 is dedicated for secrets protected by password.\n            The group 1 is dedicated for admin secrets.\n            The default vaule is 0.\n\n    -I, --identity-key IDENTITY_KEY\n            Base64 interpretation of the identity (private key) for decryption.\n            Identity key has higher priority than identity file.\n            The default value is empty.\n\n    -i, --identity-file IDENTITY_FILE\n            Selects a file from which the identity (private key) for decryption\n            is read.  The default value is ~/.ssh/id_rsa.\n\n    -v, --vault-file VAULT_FILE\n            Selects a file from/to which the vault data will be loaded/saved.\n            If an value is not specified, then instead of file will be used\n            standard input/output. The default value is empty.\n\n    -p, --password PASSWORD\n            Specyfying a password for decryption/encryption in case of group 0.\n            The default value is empty.\n\n        --help\n            Display this help and exit.\n' >&2
}

print_error_and_exit () {
    if [ "${1}" = "-h" ]; then
        PRINT_HELP=true
        shift
    else
        PRINT_HELP=false
    fi
    echo "ERROR: ${1}" >&2
    if ${PRINT_HELP}; then
        printf '\n' >&2
        print_help
    fi
    exit 1
}

get_password () {
    stty -echo
    printf "Password: "
    read PASSWORD
    stty echo
    printf '\n'
}

parse_arguments () {
    VAULT_FILE=''
    VAULT_DATA=''
    while [ $# -gt 0 ]; do
        ARGUMENT="${1}"
        shift
        case "${ARGUMENT}" in
            -d|--decrypt)
                if [ "${MODE}" = 'encrypt' ]; then
                    print_error_and_exit 'Encryt and decrypt option can not be set at the same time'
                fi
                MODE='decrypt'
                ;;
            -e|--encrypt)
                if [ "${MODE}" = 'decrypt' ]; then
                    print_error_and_exit 'Encryt and decrypt option can not be set at the same time'
                fi
                MODE='encrypt'
                ;;
            -f|--file)
                IO_FILE="${1}"
                shift
                ;;
            -g|--group)
                GROUP="${1}"
                shift
                if [ $(echo -n "${GROUP}" | grep -c -E '[^0]') -gt 0 ]; then
                    print_error_and_exit -h 'Invalid group'
                fi
                ;;
            -i|--identity-file)
                IDENTITY_FILE="${1}"
                shift
                ;;
            -p|--password)
                PASSWORD="${1}"
                shift
                ;;
            -v|--vault-file)
                VAULT_FILE="${1}"
                shift
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            -*)
                print_error_and_exit -h "\"${ARGUMENT}\" is unknown option"
                ;;
            *)
                INPUT="${ARGUMENT}"
                if [ "${1}" != "" ]; then
                    print_error_and_exit -h 'Too many arguments'
                fi
        esac
    done
    if [  "${MODE}" = "" ]; then
        MODE='decrypt'
    fi
    if [  "${MODE}" = 'encrypt' ]; then
        VAULT_DECRYPTED="${INPUT}"
    else
        if [ "${GROUP}" != "" ]; then
            print_error_and_exit 'Unexpected group parameter during decryption'
        fi
    fi
    if [ "${IDENTITY_FILE}" = "" ]; then
        IDENTITY_FILE="${HOME}/.ssh/id_rsa"
    fi
    if [ "${VAULT_FILE}" = "" ]; then
        if [ "${MODE}" = 'decrypt' ]; then
            VAULT_DATA="${INPUT}"
            if [ "${VAULT_DATA}" = "" ]; then
                print_error_and_exit -h 'Missing vault data'
            fi
        fi
    else
        if [ ! -f "${VAULT_FILE}" ]; then
            print_error_and_exit -h 'Vault file does not exists'
        fi
        VAULT_DATA="$(cat "${VAULT_FILE}")"
    fi
    if [ "${GROUP}" = "" -o "${GROUP}" = "0" ]; then
        if [ "${PASSWORD}" = "" ]; then
            get_password
        fi
        if [ "${PASSWORD}" = "" ]; then
            print_error_and_exit 'Password can not be empty'
        fi
        KEY="$(echo -n "${PASSWORD}" | shasum -a 256 | grep -o -E '^\S+')"
    else
        if [ ! -f "${IDENTITY_FILE}" ]; then
            print_error_and_exit -h 'Identity file does not exists'
        fi
    fi
}

parse_arguments "$@"
